//
//  NotesTableViewController.swift
//  iOSTestApp
//
//  Created by cahebu4 on 22.09.17.
//  Copyright © 2017 cahebu4. All rights reserved.
//

import UIKit
import CoreData

class NotesTableViewController: UITableViewController {
    
    private var notes = [Notice]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadNotesFromMemory()
        
        let button = UIBarButtonItem.init(title: "+", style: .plain, target: self, action: #selector(addNewNote))
        button.setTitleTextAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: (1.8 * UIFont.systemFontSize))], for: .normal) 
        self.navigationItem.rightBarButtonItem = button
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return notes.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "note", for: indexPath) as! NoteCell

        let currentNote = notes[indexPath.row]
        cell.title.text = currentNote.title
        cell.content.text = currentNote.content

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let sysFontSize = UIFont.systemFontSize * 0.9
        let symbolsCount = notes[indexPath.row].content!.characters.count
        let symbolsIntCount = symbolsCount.hashValue
        let symbolsWidth = CGFloat(symbolsIntCount) * sysFontSize
        var linesCount = CGFloat(lroundf(Float(symbolsWidth / UIScreen.main.bounds.width)))
        if linesCount < 1 { linesCount = 1 }
        
        return 2 * sysFontSize * (linesCount+2)
    }
    

    @objc private func addNewNote() {
        performSegue(withIdentifier: "newNoteSegue", sender: self)
    }


    func loadNotesFromMemory() -> Void {
        
        let context = CoreDataStack.persistentContainer.viewContext
        let request = NSFetchRequest<Notice>()
        let description = NSEntityDescription.entity(forEntityName: "Notice", in: context)
        request.entity = description
        
        do {
            try notes = context.fetch(request)
        } catch {
            print(error)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.loadNotesFromMemory()
        self.tableView.reloadData()
    }

}
