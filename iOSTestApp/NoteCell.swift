//
//  NoteCell.swift
//  iOSTestApp
//
//  Created by cahebu4 on 22.09.17.
//  Copyright © 2017 cahebu4. All rights reserved.
//

import UIKit

class NoteCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var content: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.content.lineBreakMode = .byWordWrapping
     }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
