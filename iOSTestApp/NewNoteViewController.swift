//
//  NewNoteViewController.swift
//  iOSTestApp
//
//  Created by cahebu4 on 22.09.17.
//  Copyright © 2017 cahebu4. All rights reserved.
//

import UIKit
import CoreData

class NewNoteViewController: UIViewController {
    
    @IBOutlet weak var newTitle: UITextField!
    @IBOutlet weak var newContent: UITextView!
    private var hasBeenSaved : Bool?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.newContent.layer.borderWidth = 1
        self.newContent.layer.borderColor = UIColor.lightGray.cgColor
        let button = UIBarButtonItem.init(title: "Сохранить", style: .plain, target: self, action: #selector(saveNote))
        self.navigationItem.rightBarButtonItem = button
        hasBeenSaved = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func saveNote() {
        
        // If notice has been saved -> Send alert and Do not save one more time        
        if hasBeenSaved! {
            showAlertWith(title: "Ошибочка :)", andMessage: "Ваша заметка уже сохранена")
            return
        }
        
        // If some field is empty -> Send alert and Do not save
        if (newTitle.text?.isEmpty)! {
            showAlertWith(title: "Недостаточно информации", andMessage: "Введите, пожалуйста, заголовок!")
            return
        } else if newContent.text.isEmpty {
            showAlertWith(title: "Недостаточно информации", andMessage: "Введите, пожалуйста, описание!")
            return
        }
        
        // If everything is OK -> Save new note
        let context = CoreDataStack.persistentContainer.viewContext
        let note : NSManagedObject = NSEntityDescription.insertNewObject(forEntityName: "Notice", into: context)
        note.setValue(newTitle.text, forKey: "title")
        note.setValue(newContent.text, forKey: "content")
        
        do {
            try context.save()
        } catch {
            print(error)
        }
        hasBeenSaved = true
    }
    
    func showAlertWith(title: String, andMessage message: String) -> Void {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
