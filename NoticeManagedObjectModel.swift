//
//  Notice+CoreDataProperties.swift
//  iOSTestApp
//
//  Created by cahebu4 on 22.09.17.
//  Copyright © 2017 cahebu4. All rights reserved.
//

import Foundation
import CoreData


extension Notice {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Notice> {
        return NSFetchRequest<Notice>(entityName: "Notice");
    }
    
    @NSManaged public var title: String?
    @NSManaged public var content: String?
    

}
